
public class Costumer {
    String nama;
    String alamat;
    int NOTELP;
    String jenisKelamin;

    public Costumer( String nama, String alamat, int NOTELP, String jenisKelamin) {

        this.nama = nama;
        this.alamat = alamat;
        this.NOTELP = NOTELP;
        this.jenisKelamin = jenisKelamin;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public int getNOTELP() {
        return NOTELP;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }
}
